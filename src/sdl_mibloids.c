#include <SDL.h>
#include <stdio.h>
#include <stdbool.h> // TODO(matt): Define my own bool type

// NOTE(matt): So this will hopefully end up being the SDL platform layer for an Asteroids clone

bool DoRender = true;
bool IsWhite = true;

bool HandleEvent(SDL_Event *Event)
{
    bool ShouldQuit = false;

    switch(Event->type)
    {
        case SDL_WINDOWEVENT:
            {
                switch(Event->window.event)
                {
                    case SDL_WINDOWEVENT_RESIZED:
                        {
                            printf("SDL_WINDOWEVENT_RESIZED (%d, %d)\n", Event->window.data1, Event->window.data2);
                            DoRender = true;
                            IsWhite = !IsWhite;
                        } break;
                }
            } break;
        case SDL_WINDOWEVENT_EXPOSED:
            {
            } break;
        case SDL_QUIT:
            {
                printf("SDL_QUIT\n");
                ShouldQuit = true;
            } break;
    }

    return(ShouldQuit);
}

int main(int argc, char *argv[])
{
    if (SDL_Init(SDL_INIT_VIDEO) != 0)
    {
        SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_INFORMATION, "SDL Init Failed", "SDL failed to initialise", 0);
    }

    SDL_Window *Window;

    Window = SDL_CreateWindow("Mibloids",
            SDL_WINDOWPOS_UNDEFINED,
            SDL_WINDOWPOS_UNDEFINED,
            640,
            480,
            SDL_WINDOW_RESIZABLE);

    if (Window)
    {

        // NOTE(matt): Create a "renderer" for the window
        SDL_Renderer *Renderer = SDL_CreateRenderer(Window,
                -1,
                0);

        if (Renderer)
        {
            for(;;)
            {
                if (DoRender)
                {
                    if (IsWhite)
                    {
                        SDL_SetRenderDrawColor(Renderer, 255, 255, 255, 255);
                    }
                    else
                    {
                        SDL_SetRenderDrawColor(Renderer, 0, 0, 0, 255);
                    }

                    SDL_RenderClear(Renderer);
                    SDL_RenderPresent(Renderer);
                }

                DoRender = false;

                SDL_Event Event;
                SDL_WaitEvent(&Event);

                if (HandleEvent(&Event))
                {
                    break;
                }
            }

        }
    }
    return(0);
    SDL_Quit();
}
